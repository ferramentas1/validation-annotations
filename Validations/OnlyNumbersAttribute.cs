﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Validations.Extensions;

namespace Validations
{
    public class OnlyNumbersAttribute : ValidationAttribute
    {
        private const string _defaultMessage = "The field {0} must be only numbers";

        public OnlyNumbersAttribute([CallerMemberName] string name = null, string friendlyName = null, string errorMessage = null)
        {
            this.SetErrorMessage(name, friendlyName, _defaultMessage, errorMessage);
        }

        public override bool IsValid(object value)
        {
            var stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue) || string.IsNullOrWhiteSpace(stringValue)) return true;

            var regex = new Regex(@"^[0-9]+$");
            return regex.IsMatch(stringValue);
        }


    }
}
