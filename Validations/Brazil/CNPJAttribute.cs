﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Validations.Extensions;

namespace Validations.Brazil
{
    public class CNPJAttribute : MaxLengthAttribute
    {
        private const string _defaultMessage = "O CNPJ {0} é inválido";

        public CNPJAttribute([CallerMemberName] string name = null, string friendlyName = null, string errorMessage = null) : base(18)
        {
            this.SetErrorMessage(name, friendlyName, _defaultMessage, errorMessage);
        }


        public override bool IsValid(object value)
        {
            var stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue) || string.IsNullOrWhiteSpace(stringValue)) return true;
            if (stringValue.Length != 14 && stringValue.Length != 18) return false;

            if (stringValue.Length == 18)
            {
                stringValue = stringValue
                    .Replace(".", string.Empty)
                    .Replace("/", string.Empty)
                    .Replace("-", string.Empty);
            }


            var regex = new Regex(@"^[0-9]{14}$");
            if (!regex.IsMatch(stringValue)) return false;

            var intArray = stringValue.Select(c => int.Parse(c.ToString())).ToArray();
            var weightsVerifyingDigit01 = new int[] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            var weightsVerifyingDigit02 = new int[] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3 };
            var sumDigit01 = 0;
            var sumDigit02 = 0;

            for(int i = 0; i < weightsVerifyingDigit01.Length; i++)
            {
                sumDigit01 += weightsVerifyingDigit01[i] * intArray[i];
                sumDigit02 += weightsVerifyingDigit02[i] * intArray[i];
            }

            var verifyingDigit01 = GetVerifyingDigit(sumDigit01);
            var verifyingDigit02 = GetVerifyingDigit(sumDigit02 + verifyingDigit01 * 2);

            return verifyingDigit01 == intArray[^2] && verifyingDigit02 == intArray[^1];
        }

        private int GetVerifyingDigit(int value)
        {
            var module = value % 11;
            return module < 2 ? 0 : 11 - module;
        }
    }
}
