﻿using System;
using System.Collections.Generic;
using System.Text;
using Validations.Brazil;
using Xunit;

namespace ValidationsTests
{
    public class CNPJTests
    {
        [Theory]
        [InlineData("", true)]
        [InlineData(" ", true)]
        [InlineData("02.371.333/0001-52", true)]
        [InlineData("62.672.193/0001-84", true)]
        [InlineData("77.556.610/0001-09", true)]
        [InlineData("18.697.910/0001-30", true)]
        [InlineData("79.296.824/0001-00", true)]
        [InlineData("57.646.854/0001-67", true)]
        [InlineData("32.128.699/0001-66", true)]
        [InlineData("05.181.948/0001-04", true)]
        [InlineData("94.078.644/0001-46", true)]
        [InlineData("34.068.330/0001-30", true)]
        [InlineData("15566529000144", true)]
        [InlineData("20401449000179", true)]
        [InlineData("90477469000109", true)]
        [InlineData("07033346000135", true)]
        [InlineData("16593968000109", true)]
        [InlineData("53071074000185", true)]
        [InlineData("21951511000169", true)]
        [InlineData("24034522000172", true)]
        [InlineData("32997749000141", true)]
        [InlineData("34028375000181", true)]
        [InlineData("52407852000100", true)] //CNPJ que estava dando erro
        [InlineData("48745", false)]
        [InlineData("5240785200010078754", false)]
        [InlineData("52407832000100", false)]
        [InlineData("5240783a000100", false)]
        [InlineData("5240783a0asfa00", false)]
        [InlineData("34128375000181", false)]
        [InlineData("34129375000181", false)]
        [InlineData("34129375020181", false)]
        [InlineData("34129375021181", false)]
        [InlineData("34129375021191", false)]
        public void CheckCNPJsGivenCNPJAndExpectedResult(string cnpj, bool expectedResult)
        {
            var validator = new CNPJAttribute();

            Assert.Equal(expectedResult, validator.IsValid(cnpj));
        }
    }
}
