﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Validations.Extensions;

namespace Validations
{
    public class FloatingPointAttribute : ValidationAttribute
    {
        private const string _defaultMessage = "The field {0} must be a floating point";
        public FloatingPointAttribute([CallerMemberName] string name = null, string friendlyName = null, string errorMessage = null)
        {
            this.SetErrorMessage(name, friendlyName, _defaultMessage, errorMessage);
        }

        public override bool IsValid(object value)
        {
            var stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue) || string.IsNullOrWhiteSpace(stringValue)) return true;

            var onlyNumbers = new Regex(@"^[0-9]+$");
            var floating = new Regex(@"^[0-9]+[.,][0-9]+$");
            return onlyNumbers.IsMatch(stringValue) || floating.IsMatch(stringValue);
        }
    }
}
