﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Validations;
using Validations.Brazil;

namespace WebTests.Models
{
    public class TestErrorsModel
    {
        [Key]
        public int Id { get; set; }

        [CPF(friendlyName: "Person's CPF", errorMessage: "The field {0} is an invalid CPF")]
        public string PersonCPF { get; set; }

        [CNPJ]
        public string CompanyCNPJ { get; set; }

        [Required, Telephone(friendlyName: "Phone Number")]
        public string PhoneNumber { get; set; }

        [OnlyNumbers]
        public string Number { get; set; }

        [MustBeTrue(friendlyName: "Must be true field")]
        public bool Checked { get; set; }
    }
}
