﻿using System;
using System.Collections.Generic;
using System.Text;
using Validations.Brazil;
using Xunit;

namespace ValidationsTests
{
    public class TelephoneTests
    {
        [Theory]
        [InlineData("", true)]
        [InlineData(" ", true)]
        [InlineData("(00)00000-0000", true)]
        [InlineData("00000000000", true)]
        [InlineData("(00)0000-0000", true)]
        [InlineData("0000000000", true)]
        [InlineData("(00)00000000", false)]
        [InlineData("000000-0000", false)]
        [InlineData("00000000000555252120", false)]
        [InlineData("0000000asa0000das555252120", false)]

        public void CheckTelephonesGivenTelephoneAndExpectedResult(string telefone, bool expectedResult)
        {
            var validator = new TelephoneAttribute();

            Assert.Equal(expectedResult, validator.IsValid(telefone));
        }
    }
}
