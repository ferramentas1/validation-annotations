﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Validations.Extensions
{
    public static class ValidationAttributeExtensions 
    {
        public static void SetErrorMessage(this ValidationAttribute attribute, string name, string friendlyName, string defaultMessage, string newErrorMessage)
        {
            var nameInMessage = friendlyName ?? name;
            var message = newErrorMessage ?? defaultMessage;

            attribute.ErrorMessage = string.Format(message, nameInMessage);
        }
    }
}
