﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;
using Validations.Extensions;

namespace Validations
{
    public class MustBeTrueAttribute : ValidationAttribute
    {
        private const string _defaultMessage = "The field {0} must be true";

        public MustBeTrueAttribute([CallerMemberName] string name = null, string friendlyName = null, string errorMessage = null)
        {
            this.SetErrorMessage(name, friendlyName, _defaultMessage, errorMessage);
        }

        public override bool IsValid(object value)
        {
            try
            {
                return (bool)value;
            }
            catch
            {
                return false;
            }
        }
    }
}
