﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Validations.Extensions;

namespace Validations.Brazil
{
    public class TelephoneAttribute : MaxLengthAttribute
    {
        private const string _defaultMessage = "O telefone {0} é inválido"; //Again, this is for Brazilion phone numbers, so, the message is in Brazilian Portuguese by default;

        public bool WithDDD { get; }

        public TelephoneAttribute(bool withDDD = true, [CallerMemberName] string name = null, string friendlyName = null, string errorMessage = null) : base(14)
        {
            WithDDD = withDDD;
            this.SetErrorMessage(name, friendlyName, _defaultMessage, errorMessage);
        }


        public override bool IsValid(object value)
        {
            var stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue) || string.IsNullOrWhiteSpace(stringValue)) return true;
            if (stringValue.Length != 11 && stringValue.Length != 10 && stringValue.Length != 14 && stringValue.Length != 13) return false;

            if(stringValue.Length == 14 || stringValue.Length == 13)
            {
                stringValue = stringValue
                    .Replace("(", string.Empty)
                    .Replace(")", string.Empty)
                    .Replace("-", string.Empty);
            }

            var regex = new Regex(@"^[0-9]{10,11}$");
            return regex.IsMatch(stringValue);
        }

    }
}
