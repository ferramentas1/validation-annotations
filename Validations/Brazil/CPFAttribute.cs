﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Validations.Extensions;

namespace Validations.Brazil
{
    public class CPFAttribute : MaxLengthAttribute
    {
        private const string _defaultMessage = "O CPF {0} é inválido"; //Since CPF is a Brazilian document, there is no sense in using default message in english =D

        public CPFAttribute([CallerMemberName] string name = null, string friendlyName = null, string errorMessage = null) : base(14)
        {
            this.SetErrorMessage(name, friendlyName, _defaultMessage, errorMessage);
        }


        public override bool IsValid(object value)
        {
            var stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue) || string.IsNullOrWhiteSpace(stringValue)) return true;
            if (stringValue.Length != 11 && stringValue.Length != 14) return false;

            if(stringValue.Length == 14)
            {
                stringValue = stringValue
                    .Replace(".", string.Empty)
                    .Replace("-", string.Empty);
            }

            var regex = new Regex(@"^[0-9]{11}$");
            if (!regex.IsMatch(stringValue)) return false;

            var sumDigit01 = 0;
            var sumDigit02 = 0;
            var e = 0;

            var intArray = stringValue.Select(c => int.Parse(c.ToString())).ToArray();

            for (int i = 10; i >= 2; i--, e++)
            {
                var digitAsInt = intArray[e];
                sumDigit01 += digitAsInt * i;
                sumDigit02 += digitAsInt * (i + 1);
            }

            var verifyingDigit01 = GetVerifyingDigit(sumDigit01);
            var verifyingDigit02 = GetVerifyingDigit(sumDigit02 + verifyingDigit01 * 2);

            return verifyingDigit01 == intArray[^2] && verifyingDigit02 == intArray[^1];
        }

        private int GetVerifyingDigit(int sum)
        {
            var module = sum % 11;

            return module < 2 ? 0 : 11 - module;
        }
    }
}
