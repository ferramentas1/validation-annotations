﻿using System;
using System.Collections.Generic;
using System.Text;
using Validations.Brazil;
using Xunit;

namespace ValidationsTests
{
    public class CPFTests
    {
        [Theory]
        [InlineData("", true)]
        [InlineData(" ", true)]
        [InlineData("095.318.440-46", true)]
        [InlineData("411.040.210-72", true)]
        [InlineData("489.610.990-27", true)]
        [InlineData("114.743.070-51", true)]
        [InlineData("823.709.970-28", true)]
        [InlineData("91964581001", true)]
        [InlineData("83440395090", true)]
        [InlineData("86237648038", true)]
        [InlineData("93494209022", true)]
        [InlineData("17635043013", true)]
        [InlineData("823.709.970-88", false)]
        [InlineData("823.709.970--8", false)]
        [InlineData("823.709.97o-28", false)]
        [InlineData("93494a09022", false)]
        [InlineData("934942090225", false)]
        public void CheckValidCPFsGivenCPFAndExpectedResult(string cpf, bool expectedResult)
        {
            var validator = new CPFAttribute();

            Assert.Equal(expectedResult, validator.IsValid(cpf));
        }
    }
}
