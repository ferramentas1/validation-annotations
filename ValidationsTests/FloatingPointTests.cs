﻿using System;
using System.Collections.Generic;
using System.Text;
using Validations;
using Xunit;

namespace ValidationsTests
{
    public class FloatingPointTests
    {
        [Theory]
        [InlineData("", true)]
        [InlineData("20", true)]
        [InlineData("20,02", true)]
        [InlineData("2030.84", true)]
        [InlineData("20,303,5", false)]
        [InlineData("30,,25", false)]
        [InlineData("30..98", false)]
        [InlineData("a", false)]
        [InlineData("30,ab", false)]
        public void TestValidationGivenStringAndExpectedResult(string value, bool expectedResult)
        {
            Assert.Equal(expectedResult, new FloatingPointAttribute().IsValid(value));
        }
    }
}
