﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using WebTests.Models;

namespace WebTests
{
    public class Contexto : DbContext
    {
        public DbSet<TestErrorsModel> CustomAnnotations { get; set; }

        public Contexto([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public Contexto()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;

            var user = Environment.GetEnvironmentVariable("UsuarioSQLServer", EnvironmentVariableTarget.User);
            var password = Environment.GetEnvironmentVariable("SenhaSQLServer", EnvironmentVariableTarget.User);
            var server = Environment.GetEnvironmentVariable("ServidorSQLServer", EnvironmentVariableTarget.User);
            var database = "AnnotationsTests";

            optionsBuilder.UseSqlServer($"Server={server};Database={database};User Id={user};Password={password};");
        }
    }
}
