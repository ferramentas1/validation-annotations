﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Validations;
using Xunit;
using Xunit.Sdk;

namespace ValidationsTests
{
    public class OnlyNumbersTests
    {
        [Theory]
        [InlineData("", true)]
        [InlineData("0487", true)]
        [InlineData("04444848466784454785", true)]
        [InlineData("a4754", false)]
        [InlineData("4578a45787", false)]
        [InlineData("455787a", false)]
        [InlineData("a", false)]
        [InlineData("as15451sas78847", false)]
        public void TestValidationGivenStringAndExpectedResult(string value, bool expectedResult)
        {
            Assert.Equal(expectedResult, new OnlyNumbersAttribute().IsValid(value));
        }
    }
}
